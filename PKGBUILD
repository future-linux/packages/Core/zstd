# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=zstd
pkgver=1.5.6
pkgrel=2
pkgdesc="Zstandard - Fast real-time compression algorithm"
arch=('x86_64')
url="https://facebook.github.io/zstd/"
license=('BSD-3-Clause' 'GPL-2.0-only')
depends=('glibc' 'gcc-libs' 'zlib' 'xz' 'lz4')
makedepends=('cmake' 'ninja')
source=(https://github.com/facebook/zstd/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha256sums=(8c29e06cf42aacc1eafc4077ae2ec6c6fcb96a626157e0593d5e82a34fd403c1)

prepare() {
	cd ${pkgname}-${pkgver}

	# avoid error on tests without static libs, we use LD_LIBRARY_PATH
	sed '/build static library to build tests/d' -i build/cmake/CMakeLists.txt
	sed 's/libzstd_static/libzstd_shared/g' -i build/cmake/tests/CMakeLists.txt
}

build() {
	cd ${pkgname}-${pkgver}

	export CFLAGS+=' -ffat-lto-objects'
	export CXXFLAGS+=' -ffat-lto-objects'

	cmake -S build/cmake -B build -G Ninja \
	      -DCMAKE_BUILD_TYPE=None          \
	      -DCMAKE_INSTALL_PREFIX=/usr      \
	      -DZSTD_ZLIB_SUPPORT=ON           \
	      -DZSTD_LZMA_SUPPORT=ON           \
	      -DZSTD_LZ4_SUPPORT=ON            \
	      -DZSTD_BUILD_CONTRIB=ON          \
	      -DZSTD_BUILD_STATIC=OFF          \
	      -DZSTD_BUILD_TESTS=ON            \
	      -DZSTD_PROGRAMS_LINK_SHARED=ON

	cmake --build build

}

package() {
	cd ${pkgname}-${pkgver}

	DESTDIR=${pkgdir} cmake --install build
}
